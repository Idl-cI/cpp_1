#include<iostream>
#include<vector>
#include<cmath>

#define DEBUG 

#define AIR_RESISTANCE 

#ifdef DEBUG 
#endif

using std::endl;
using std::cout;
using std::vector;


struct SimulationObject {
        Pair<double> * arrays[3];
        void print();
};


//pair coordinate (x,y)
template <typename T>
struct Pair {
    T x;
    T y;
    Pair<T> (const T& a, const T& b): /*todo*/ {
        #ifdef DEBUG
        cout << "Pair created " << *this << endl;
        #endif
    }
    ~Pair<T> () {
        #ifdef DEBUG
        cout << "Pair destroyed" << endl;
        #endif
    }
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Pair<T>& pair) {
    os << "(" << pair.x << ", " << pair.y << ")";
    return os;
}


//What would you need to do to make Particle a class with same functionality? (set/get-methods, etc.)

struct Particle: public SimulationObject {
    
    public:
        //Particle(Pair<double> pos, Pair<double> arrays[VEL]);  Zusatz, freiwillig. Vgl. Würfe aus dem Physikunterricht.
        Particle(const Pair<double>& pos);
        Particle(const Pair<double>& pos, const Pair<double>& vel, const Pair<double>& acl);
        ~Particle();

        //print() ?

        int POS = 0;
        int VEL = 1;
        int ACL = 2;

        DATATYPE counter; //change DATATYPE until it compiles

        
};

Particle::Particle(const Pair<double>& pos) {
    //todo
    //increase counter
    Particle::counter += 1;

    //fill arrays dynamically
    //todo

    cout << "Particle created : " <<  endl;
    this->print();
}

Particle::Particle(const Pair<double>& pos, const Pair<double>& vel, const Pair<double>& acl) {
        //todo
}



Particle::~Particle() {
    //deallocate arrays etc. correctly!
    //todo

    Particle::counter -= 1;

    cout << "Particle destroyed. " << counter << " particles left." << endl;
    
}


class Simulation {
    std::vector<Particle*> p_array;
    double timestep;
    
    const double g = 10.0;

    const double c_air = 0.05;

    public:
        Simulation();
        Simulation(double t);

        void addParticle(const Particle& particle);

        void operator += (Particle p);
        void operator -= (Particle p);

        void update();
        void setTimestep(const double& t);
};

Simulation::Simulation() {
    timestep = 0.02;
    p_array.reserve(5);
}

Simulation::Simulation(double t) {
    timestep = t;
    p_array.reserve(5);
}

void Simulation::addParticle(const Particle& particle) {
    Particle* p = new Particle(particle);  // Assuming Particle has a copy constructor
    p->arrays[particle.ACL]->y = -g;
    
    /*todo*/
}



void Simulation::update() {
    int POS = 0;
    int VEL = 1;
    int ACL = 2;

    for (/*todo ohne auto*/) {
        cout << "Particle at " << *((*p_i)->arrays[POS]) << endl;

        (*p_i)->arrays[POS]->x += timestep * (*p_i)->arrays[VEL]->x;
        (*p_i)->arrays[POS]->y += timestep * (*p_i)->arrays[VEL]->y;

        (*p_i)->arrays[VEL]->x += timestep * (*p_i)->arrays[ACL]->x;
        (*p_i)->arrays[VEL]->y += timestep * (*p_i)->arrays[ACL]->y;


        /*
        print position of all particles to second input argument of main. in one line.
        */

        if ((*p_i)->arrays[POS]->y <= 0.00) {
            delete (*p_i);
        }

    }
}
