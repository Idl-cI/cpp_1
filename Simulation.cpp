#include<iostream>
#include"Particle.hpp"

/*
1. Initialisierung der Pair-Klasse (Pair.h):

    Vervollständige den Konstruktor der Pair-Klasse, um ihre Mitglieder x und y ordnungsgemäß zu initialisieren.
2. Initialisierung der Particle-Klasse (Particle.h):

    Implementiere den Konstruktor für die Particle-Klasse, der ein Pair<double> als Parameter akzeptiert. Stelle sicher, dass er den counter erhöht und dynamisch Speicher für die Arrays alloziert.

3.Destruktor der Particle-Klasse (Particle.h):

    Implementiere den Destruktor für die Particle-Klasse. Stelle sicher, dass er jeglichen dynamisch allokierten Speicher freigibt und den counter ordnungsgemäß aktualisiert.
4. Methode addParticle der Simulation-Klasse (Simulation.h):

    Vervollständige die Methode addParticle in der Simulation-Klasse. Stelle sicher, dass sie ein neues Particle-Objekt korrekt erstellt, die Erdbeschleunigung in den Arrays setzt und es zum p_array hinzufügt

5. Methode update der Simulation-Klasse (Simulation.h):

    Vervollständige die Methode update in der Simulation-Klasse. Iteriere über die Partikel im p_array, aktualisiere ihre Positionen und Geschwindigkeiten gemäß der gegebenen Physik. Wenn die y-Koordinate eines Partikels kleiner oder gleich 0,00 wird, lösche das Partikel.
    Newtonsche Mechanik: acceleration = d(velocity)/d(time) = d*d*(position)/d*d*(time)

6. Nutze Kommandozeilenargumente, um einzulesen und die Ergebnisse zu loggen.

(Zusatz:)
7. Luftwiderstand miteinbeziehen.

8.  

*/
using namespace std;


unsigned int Particle::counter = 0;


int main(/*todo*/) {

    Simulation sim;

    Particle p1 = Particle(Pair<double>(0,10));
    Particle p2 = Particle(Pair<double>(1,7));
    Particle p3 = Particle(Pair<double>(1,3));


    sim.addParticle(p1);
    sim.addParticle(p2);
    sim.addParticle(p3);

    /*todo*/

    while(Particle::counter > 0) {
        cout << "Counter: " << Particle::counter << endl;
        sim.update();
    }

    return 0;

}

